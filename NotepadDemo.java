import java.awt.*;
import java.awt.event.*;
import java.io.*;

class NotepadDemo extends Frame implements ActionListener{
    NotepadDemo obj = this;
    Menu fileMenu,editMenu;
    MenuItem newWindow,saveAs;
    TextArea ta;
    Button b;
    OpenItem open;
    SaveItem save;
    ExitItem exit;
    ReplaceItem replace;
    FindItem find;
    NewItem new1;
    ExitDialog obj1;
     
    NotepadDemo(String title){
        super(title);
        setSize(800,800);
        setVisible(true);

        setLayout(new BorderLayout());
        ta = new TextArea();
        add(ta, BorderLayout.CENTER);

        MenuBar mbar = new MenuBar();
        setMenuBar(mbar);

        fileMenu = new Menu("File");
        editMenu = new Menu("Edit");
        mbar.add(fileMenu);
        mbar.add(editMenu);

        open = new OpenItem("Open...",this,ta);
        new1  = new NewItem("New",ta);
        newWindow = new MenuItem("New Window");
        
        save = new SaveItem("Save",this,ta);
        exit = new ExitItem("Exit");


        fileMenu.add(new1);
        fileMenu.add(newWindow);
        fileMenu.add(open);
        fileMenu.add(save);
        fileMenu.add(new MenuItem("-"));
        fileMenu.add(new MenuItem("-"));
        fileMenu.add(exit);

        find = new FindItem("Find",this,ta);
        replace = new ReplaceItem("Replace...",this,ta);
 
        editMenu.add(new MenuItem("-"));
        editMenu.add(find);
        editMenu.add(new MenuItem("-"));
        editMenu.add(replace);

        editMenu.add(new MenuItem("-"));

                
        fileMenu.addActionListener(this);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
                if((ta.getText()).equals("")){
                    System.exit(0);
                }else{
                    new ExitDialog(obj,"Notepad",ta);
                }
            }
        });   
    }    
        public void actionPerformed(ActionEvent ae){}
}    

