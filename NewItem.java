import java.awt.event.*;
import java.awt.*;
public class NewItem extends MenuItem implements ActionListener{
    TextArea ta;
    NewItem(String title,TextArea ta){
        super(title);
        this.ta = ta;
        addActionListener(this);
    }
    public void actionPerformed(ActionEvent ae){
        ta.setText(" ");
        //System.out.println("Hiiiii");
    }
}