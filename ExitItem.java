import java.awt.event.*;
import java.awt.*;
import java.io.*;
public class ExitItem extends MenuItem implements ActionListener{
    ExitItem(String title){
        super(title);
        addActionListener(this);
    }
    public void actionPerformed(ActionEvent ae){
        System.exit(0);
    }    
}    