import java.awt.event.*;
import java.awt.*;
import java.io.*;
public class OpenItem extends MenuItem implements ActionListener{
    Frame parent;
    TextArea ta;
    OpenItem(String title, Frame parent, TextArea ta){
        super(title);
        this.parent = parent;
        this.ta = ta;
        addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent ae){
        FileDialog fd = new FileDialog(parent,"File Dialog.....", FileDialog.LOAD);
        fd.setVisible(true);
        if(fd.getDirectory() != null && fd.getFile() != null){
            System.out.println(fd.getDirectory() + "FILE: " + fd.getFile());
            File f = new File(fd.getDirectory() + fd.getFile());
            try{
                FileInputStream fin = new FileInputStream(f);
                BufferedReader br = new BufferedReader(new InputStreamReader(fin));
                String line;
                while((line = br.readLine())!= null){
                ta.append(line + "\n");
                //System.out.println(line);
            }
            System.out.println();
            br.close();
            fin.close();                
		    }catch(FileNotFoundException fe){
			    System.out.println("File Nahi Mila!!");
		    }catch(IOException e){
			    System.out.println("IOE Caught!");
		    }
        }
    }
}    