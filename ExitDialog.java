import java.awt.event.*;
import java.awt.*;
class ExitDialog extends Dialog implements ActionListener{
   Frame parent;
   Label l1;
   Button b1,b2,b3;
   TextArea ta;
   ExitDialog(Frame parent,String title,TextArea ta){
        super(parent,title);
        this.ta =  ta;
        setLayout(new FlowLayout());
        l1 = new Label("Do you want to save changes to untitled?");
        add(l1);
        b1 = new Button("Save");
        add(b1);
        b2 = new Button("Don't Save");
        add(b2);
        b3 = new Button("Cancel");
        add(b3);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
                dispose();
            }
        });
        setSize(440,250);
        setVisible(true);
    }
     
    public void actionPerformed(ActionEvent ae){
        
        String str = ae.getActionCommand();
        if(str.equals("Save")){
             SaveItem s = new SaveItem("Save",parent,ta);
             s.actionPerformed(ae);
            System.exit(0);
            
        }
        if(str.equals("Don't Save")){
            System.exit(0);
        }
        if(str.equals("Cancel")){
            dispose();
        }
    }
}
  