import java.awt.event.*;
import java.awt.*;
public class HelperFunctions{
    public void find(String findStr, TextArea ta, String textToBeSearched){
        int index = 0;
        String fullText;
        try{
            fullText = ta.getText();
            index = fullText.indexOf(textToBeSearched);
            if(index != -1){
                ta.requestFocusInWindow();
                ta.select(index, index + textToBeSearched.length());
            }
            else{
                // new NotFoundModal(null, findStr, textToBeSearched);
                System.out.println("Cannot find " + textToBeSearched);
            }
        }catch(Exception e){
            System.out.println("Caught");
        }
    }
}
    