import java.awt.event.*;
import java.awt.*;
import java.io.*;
public class SaveItem extends MenuItem implements ActionListener{
    Frame parent;
    TextArea ta;
    SaveItem(String title, Frame parent,TextArea ta){
        super(title);
        this.parent = parent;
        this.ta = ta;
        addActionListener(this);
    }
    public void actionPerformed(ActionEvent ae){
        FileDialog fd1 = new FileDialog(parent,"File Dialog.....", FileDialog.SAVE);
            fd1.setVisible(true);
            if(fd1.getDirectory() != null && fd1.getFile() != null){
                //System.out.println(fd1.getDirectory() + "FILE: " + fd1.getFile());
                File f1 = new File(fd1.getFile());
                try{
                    FileOutputStream fos = new FileOutputStream(f1);
                    String fileSave = ta.getText();
                    //System.out.println(fileSave.getBytes());
                    byte b[] = fileSave.getBytes();
                    fos.write(b);
                    fos.close();
                }catch(IOException e){
                     System.out.println("IOE caught!");
                }
            }
        }
    }   