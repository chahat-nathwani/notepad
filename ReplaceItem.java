import java.awt.event.*;
import java.awt.*;
class SampleDialog extends Dialog implements ActionListener{
    Label labelFindWhat,labelReplaceWith;
    TextField t1,t2;
    Button b1,b2,b3,b4;
    Checkbox c1,c2;
    TextArea ta;
    SampleDialog(Frame parent, String title,TextArea ta){
        super(parent,title,true);
        // System.out.println("inside replace dialog");
        this.ta = ta;
        setLayout(null);
        labelFindWhat = new Label("Find what: ");
        add(labelFindWhat);
        labelFindWhat.setBounds(15,50,70,20);
        t1 = new TextField(30);
        add(t1);
        t1.setBounds(110,50,200,20);
        labelReplaceWith = new Label("Replace with: ");
        add(labelReplaceWith);
        labelReplaceWith.setBounds(15,80,80,20);
        t2 = new TextField(30);
        add(t2);
        t2.setBounds(110,80,200,20);
        b1 = new Button("Replace");
        add(b1);
        b1.setBounds(320,76,100,25);
        b2 = new Button("Cancel");
        add(b2);
        b2.setBounds(320,136,100,25);
        b1.addActionListener(this);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
                dispose();
            }
        });
        setSize(440,250);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent ae){
        String str = ae.getActionCommand();
        if(str.equals("Replace")){
            // System.out.println("replace button clicked");
            String findWhat = t1.getText();
            // System.out.println(findWhat);
            String replaceWith = t2.getText();
            // System.out.println(replaceWith);
            String fullText = ta.getText();
            // System.out.println(fullText);
            fullText.replaceAll(findWhat,replaceWith);
            ta.setText(fullText); 
            // System.out.println(ta.getText());   
        }
        if(str.equals("Cancel")){
            dispose();
        }
    }
}

class ReplaceItem extends MenuItem implements ActionListener{
    //Button b;
    Frame parent;
    TextArea ta;
    //String FindWhatText = t1.getText();
    ReplaceItem(String title, Frame parent,TextArea ta){
        super(title);
        this.parent = parent;
        this.ta = ta;
        addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent ae){
        SampleDialog dialog = new SampleDialog(parent,"Replace",ta);  
        // dialog.actionPerformed(ae); 
    }
}    

    