import java.awt.event.*;
import java.awt.*;
class SampleFindDialog extends Dialog implements ActionListener{
    TextArea ta;
    Label l1,l2,l3;
    TextField t1,t2;
    Button btnFind,btnCancel;
    Checkbox c1,c2,c3,c4;
    CheckboxGroup cbg;
    String textToBeSearched,givenString;
    SampleFindDialog(Frame parent, String title, TextArea ta){
        super(parent,title,true);
        this.ta = ta;
        setLayout(null);
        l1 = new Label("Find what: ");
        add(l1);
        l1.setBounds(15,50,70,20);
        t1 = new TextField(30);
        add(t1);
        t1.setBounds(110,50,200,20);
        btnFind = new Button("Find Next");
        add(btnFind);
        btnFind.setBounds(320,46,100,25);
        btnCancel = new Button("Cancel");
        add(btnCancel);
        btnCancel.setBounds(320,76,100,25);
        btnFind.addActionListener(this);
        btnCancel.addActionListener(this);
        givenString = ta.getSelectedText();
        addWindowListener(new WindowAdapter(){
            public void windowOpened(WindowEvent we){
                t1.setText(textToBeSearched);
            }
            public void windowClosing(WindowEvent we){
                dispose();
            }
        });
        setSize(440,210);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent ae){
        HelperFunctions helper = new HelperFunctions();
        String textToBeSearched = t1.getText();
        String str = ae.getActionCommand();
        if(str.equals("Find Next")){
            // System.out.println("Inside find next!");
                helper.find(givenString,ta,textToBeSearched);
            }
        if(str.equals("Cancel")){
            dispose();
        }
    }
}

class FindItem extends MenuItem implements ActionListener{
    Frame parent;
    TextArea ta;
    FindItem(String title, Frame parent, TextArea ta){
        super(title);
        this.parent = parent;
        this.ta = ta;
        addActionListener(this);        
    }
      
    public void actionPerformed(ActionEvent ae){
       SampleFindDialog dialog = new SampleFindDialog(parent,"Find",ta);
       }

}    

    